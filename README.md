# Academic Integrity in Space
Copyright (C) 2020 Ryerson University

[https://www.ryerson.ca/academicintegrity/](https://www.ryerson.ca/academicintegrity/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but __without any warranty__; without even the implied warranty of __merchantability__ or __fitness for a particular purpose__. For more details see [GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.html).
## Deployment
1. Download and unzip the files.
2. Upload them to your server.
3. Update `<base>` tag in the `index.html` to match the new path.
## Updating Configuration File
You can customise many ascpects of the game by modifying the `_/game_data.json` file.
### About, Terms of Use and Contacts ###
The content of these text boxes is defined inside the `gameinfo` property of the `_/game_data.json` file. You can change it by modifying the corresponding HTML strings.
### Game Into Slides ###
The content of the game intro slides is defined in the `intro` array of the `_/game_data.json` file. Each slide object has the following structure:
```
{
    "image": "path/to/slide/image.png",
    "text": "<p>Slide text in HTML format</p>"
}
```
### Configuring Planets
You can control the number and look of planets by modifying the `planets` array inside the `_/game_data.json` file. Each planet object has the following structure:
```
{
    "name": "Planet Name",
    "image": {
        "normal": "path/to/normal/planet/image.png",
        "dark": "path/to/defeated/planet/image.png"
    },
    "music": "path/to/background/music/loop.mp3",
    "background": {
        "day": "path/to/daytime/lanscape/image.png",
        "night": "path/to/nighttime/landscape/image.png"
    },
    "info": "<p>Planet intro in HTML format</p>",
    "summary": {
        "victory": {
            "message": "<p>Victory message in HTML format</p>",
            "image": "path/to/victory/image.png"
        },
        "defeat": {
            "message": "<p>Defeat message in HTML format</p>",
            "image": "path/to/defeat/image.png"
        }
    }
}
```
### Adding Characters
You can add characters and configure their questions and answers by modifying the `characters` array inside the `_/game_data.json` file. A character object has the following structure:
```
{
    "name": "Character Name",
    "image": "path/to/character/image.png",
    "planet": 0,
    "allegiance": 0,
    "questions":[
        ...
    ],
    "boss": [
        ...
    ]
}
```
The `planet` property should have the ID of the planet you want this character to appear on. The default value of the `allegiance` property should always be 0.
#### Character Questions ####
Each character can have an unlimited number of questions. The questions should be added to the `questions` array in the character object.

The questions can be arranged in a __branching scenario__ by providing a __different question ID__ in the `next` property of each answer option. 

To go to the next character, or to the Boss battle after the last character set the `next` property of the last answer to `null`.

Below is a sample structure of a question object:
```
{
    "id": 1,
    "question": "Question text in plain text format",
    "voiceover": "path/to/voiceover/file.mp3",
    "hints": [
        "<p>Spacebot message in HTML format</p>",
        "<p><em>Chat Character 1:</em> Hi</p><p><em>Chat Character 2:</em> Hi. How are you?</p>",
        "<p>Orb's prophecy</p>"
    ],
    "options": [
        {
            "label": "Yes",
            "correct": 0,
            "feedback": "<p>Incorrect feedback in HTML format</p>",
            "next": 3
        },
        {
            "label": "No",
            "correct": 1,
            "feedback": "<p>Correct feedback in HTML format</p>",
            "next": 2
        }
    ]
}
```
#### Boss Battle Questions ####
Each character can have one or more questions for the Boss battle. If a character was "lost" to the Boss during the game it can be won back during the Boss battle by answering a question from this array.

Note that answer options to boss questions do not have the `next` property. The questions in the Boss battle are determined by the characters you have lost to the Boss during the game.
```
{
   "question": "Question for the Boss Battle in plain text format",
   "options": [
        {
            "label": "Yes",
            "correct": 1,
            "feedback": "<p>Correct feedback in HTML format</p>"
        },
        {
            "label": "No",
            "correct": 0,
            "feedback": "<p>Incorrect feedback in HTML format</p>"
        }
   ]
}
```