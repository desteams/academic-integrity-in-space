/************************************************************************
 *
 * ACADEMIC INTEGRITY IN SPACE
 * Copyright (C) 2020 Ryerson University
 * <https://www.ryerson.ca/academicintegrity>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. For more details
 * see <https://www.gnu.org/licenses/gpl-3.0.html>
 *
 ***********************************************************************/

var utils = angular.module('utils', ['ngCookies']),
	app = angular.module('aio5', ['ngRoute', 'ngSanitize', 'utils']),
	api;

app.config(['$routeProvider', '$locationProvider', 
	
	function($routeProvider, $locationProvider) {
		$routeProvider.when('/', {
			templateUrl : '_/tpl/splash.tpl.html',
			controller  : 'splashCtrl'
		}).when('/intro/', {
			templateUrl : '_/tpl/intro.tpl.html',
			controller  : 'introCtrl'
		}).when('/gameinfo/:page?', {
			templateUrl : '_/tpl/gameinfo.tpl.html',
			controller  : 'gameInfoCtrl'
		}).when('/settings/', {
			templateUrl : '_/tpl/settings.tpl.html',
			controller  : 'settingsCtrl'
		}).when('/space/', {
			templateUrl : '_/tpl/space.tpl.html',
			controller  : 'spaceCtrl'
		}).when('/planet/:index?', {
			templateUrl : '_/tpl/planet.tpl.html',
			controller  : 'planetCtrl'
		}).when('/planet/info/:index', {
			templateUrl : '_/tpl/planetinfo.tpl.html',
			controller  : 'planetInfoCtrl'
		}).when('/planet/:index/boss', {
			templateUrl : '_/tpl/boss.tpl.html',
			controller  : 'bossCtrl'
		});
	}
	
]).run(['$rootScope', '$http', '$location', '$storage', '$route', '$interval', '$timeout',
		
	function($rootScope, $http, $location, $storage, $route, $interval, $timeout) {
		
		$rootScope = _.extend($rootScope, {
			intro: [],
			howto: [],
			planets: {},
			tools: {},
			stories: {},
			boss: false
		});
		
		$rootScope.gameStarted = false;
		
		$rootScope.aio = _.extend( {
				'uuid': generateUUID(),
				"fullscreen": false,
				"sound": {
					"music": true,
					"speech": true
				},
				"planets" : []
			},
			$storage.getObject(app.name) || {}
		);
		
		$rootScope.saveState = function() {
			$storage.setObject(app.name, $rootScope.aio);
		};
		
		arrayFillMethod();

		$http.defaults.cache = true;
		$http.get('_/game_data.json')
			.success(function(data, status, headers, config) {
				$rootScope.gameinfo = data.gameinfo;
				$rootScope.intro = data.intro;
				$rootScope.planets = data.planets;
				$rootScope.tools = data.tools;
				$rootScope.characters = data.characters;
				$rootScope.assetsLoaded = false;
				$rootScope.imagesProgress = 'Loading...';
				_.each($rootScope.planets, function(planet, planetIndex, planets) {
					var characters;
					characters = _.where($rootScope.characters, {planet: planetIndex});
															
					if (!$rootScope.aio.planets[planetIndex]) {
						$rootScope.aio.planets.push({
							name: planet.name,
							completed: false,
							status: 0,
							allegiance: new Array(characters.length).fill(0),
							currentCharacter: 0,
							toolsUsed: 0
						});
					}
				});
				$rootScope.saveState();
				loadIntroAssets();
			})
			.error(function(data, status, headers, config) {
				console.log('error: ' + status);
			});
				
		$rootScope.setLocation = function(path) {
			$location.path(path).replace();
		};
		
		$rootScope.openSettings = function() {
			
			
			if (window.voiceSound) { 
				window.voiceSound.stop();
			}
			
			$rootScope.makeClickSound();
			
			$rootScope.returnPath = $location.$$url;
			$rootScope.setLocation('/settings');
		}
		
		$rootScope.closeSettings = function() {
			
			$rootScope.makeClickSound();
			
			if ($rootScope.returnPath) {
				$rootScope.setLocation($rootScope.returnPath);
				$rootScope.returnPath = "";
			} else {
				$rootScope.setLocation('/space');
			}
		}
		
		$rootScope.openLocation = function(path) {
			
			if (window.voiceSound) { 
				window.voiceSound.stop();
			}
			
			$rootScope.makeClickSound();
			
			if (!$rootScope.returnPath) {
				$rootScope.returnPath = $location.$$url;
			}
			$rootScope.setLocation(path);
		}
		
		$rootScope.closeLocation = function() {
			
			$rootScope.makeClickSound();
			
			if ($rootScope.returnPath) {
				$rootScope.setLocation($rootScope.returnPath);
				$rootScope.returnPath = undefined;
			} else {
				$rootScope.setLocation('/space');
			}
		}
		
		function loadIntroAssets() {
			var queue, imgArray, sounds, loadingProgress;
			
			queue = new createjs.LoadQueue();
			queue.installPlugin(createjs.Sound);
			assets = [];
			sounds = [
				{id:"space", src:"_/snd/intro_daniil_128k.mp3"},
				{id:"click", src:"_/snd/click.mp3"},
				{id:"cheer", src:"_/snd/cheer.mp3"},
				{id:"boo", src:"_/snd/boo.mp3"},
				{id:"laughter", src:"_/snd/laughter_01.mp3"},
				{id:"anger", src:"_/snd/anger_02.mp3"},
				{id:"victory", src:"_/snd/cheer.mp3"}
			];
			
			_.each($rootScope.intro, function(el, ind, list) {
				assets.push({id: "intro_" + ind, src: el.image});
			});
			
			_.each($rootScope.planets, function(el, ind, list) {
				assets.push({id: "planet_" + ind, src: el.image});
			});
			
			assets.push({id: "boss", src: "_/img/boss.png"});
			
			assets = assets.concat(sounds);
			
			queue.on("complete", function() { 
				$rootScope.introAssetsLoaded = true;
				$rootScope.$apply();
				$rootScope.focusElement("#btnStart");
			}, this);
			
			$rootScope.loaded = 0;
			
			loadingProgress = $interval(function() {
				$rootScope.loaded = queue.progress;
				console.log($rootScope.loaded);
				
				if (queue.progress >= 1 && angular.isDefined(loadingProgress)) {
					$interval.cancel(loadingProgress);
					$rootScope.loaded = 1;
					loadingProgress = null;
				}
			});
			
			queue.loadManifest(assets);
		}
		
		$rootScope.loadPlanetAssets = function(n) {
			var queue, planet, aliens, assets, loadingProgress;
			
			queue = new createjs.LoadQueue();
			queue.installPlugin(createjs.Sound);
			planet = $rootScope.planets[n];
			aliens = _.where($rootScope.characters, {planet: n});
			assets = [];
			assets.push({id: "landscape_day_" + n, src: planet.background.day});
			assets.push({id: "landscape_night_" + n, src: planet.background.night});
			if(planet.music.length) {
				assets.push({id: planet.name, src: planet.music});
			}
			_.each(aliens, function(alien, ind) {
				assets.push({id: "alien_" + ind, src: alien.image});
				_.each(alien.questions, function(q) {
					if (q.voiceover) {
						assets.push({ id: alien.name + "_" + alien.planet + "_" + q.id, src: q.voiceover});
					};
				});
			});
			queue.on("complete", function() {
				planet.assetsLoaded = true;
				$rootScope.$apply();
			}, this);
			
			$rootScope.loaded = 0;
			
			loadingProgress = $interval(function() {
				$rootScope.loaded = queue.progress;
				if (queue.progress >= 1 && angular.isDefined(loadingProgress)) {
					$interval.cancel(loadingProgress);
					$rootScope.loaded = 1;
					loadingProgress = null;
				}
			});
			
			queue.loadManifest(assets);
		}
		
		$rootScope.makeClickSound = function() {
			if (window.clickSound) { 
				window.clickSound.play();
			} else {
				window.clickSound = createjs.Sound.play('click');
			}
		};
		
		$rootScope.ambientSoundName = 'space';
		
		$rootScope.playAmbientSound = function(reload) {
			var props = new createjs.PlayPropsConfig().set({
				loop: -1, 
				volume: 0.1
			});
			if(window.ambientSound) {
				if(reload) {
					//console.log('reload');
					window.ambientSound.stop();
					window.ambientSound = null;
					window.ambientSound = createjs.Sound.play($rootScope.ambientSoundName, props);
				} else {
					//console.log('resume');
					window.ambientSound.play();
				}
			} else {
				//console.log('load');
				window.ambientSound = createjs.Sound.play($rootScope.ambientSoundName, props);
			}
		};
		
		$rootScope.stopAmbientSound = function() {
			if (window.ambientSound) {
				window.ambientSound.stop();
			}
		};
		
		$rootScope.toggleMusic = function(event) {
			
			$rootScope.makeClickSound();
			
			if(event.type == 'click' || event.keyCode == 13 || event.keyCode == 32) {
			
				if($rootScope.aio.sound.music) {
					$rootScope.stopAmbientSound();
				} else if($rootScope.ambientSoundName.length) {
					$rootScope.playAmbientSound(true);
				}
				$rootScope.aio.sound.music = !$rootScope.aio.sound.music;
				$rootScope.saveState();
			}
		}
		
		$rootScope.toggleSpeech = function(event) {
			
			$rootScope.makeClickSound();
			
			if(event.type == 'click' || event.keyCode == 13 || event.keyCode == 32) {
			
				$rootScope.aio.sound.speech = !$rootScope.aio.sound.speech;
				$rootScope.saveState();
			}
		}
		
		$rootScope.focusElement = function(selector) {
			$timeout( function(){
				if (typeof selector == 'string') {
					$(selector).focus();
				} else {
					selector.focus();
				}
			}, 300);
		};
		
		$rootScope.toggleDialogFocus = function(overlay, show) {
			var $overlay;
			$overlay = $(overlay);
			if ( show ) {
				$rootScope.focusElement( $overlay.find('.dialog') );
				$rootScope.trapFocus(overlay);
			}
		};
		
		$rootScope.trapFocus = function(overlay){
			overlay = document.querySelector(overlay);
			//console.log(overlay);
			if (overlay) {
				$rootScope.focusable = overlay.querySelectorAll("button, a, *[tabindex]");
				$rootScope.focusFirst = $rootScope.focusable[0];
				$rootScope.focusLast = $rootScope.focusable[$rootScope.focusable.length - 1];
				
				overlay.addEventListener("keydown", function(event) {
					if (event.key === "Tab" || event.keyCode === 9) {
						
						if(event.shiftKey) {
							if (document.activeElement === $rootScope.focusFirst) {
								$rootScope.focusLast.focus();
								event.preventDefault();
							}
						} else {
							if (document.activeElement === $rootScope.focusLast) {
								$rootScope.focusFirst.focus();
								event.preventDefault();
							}
						}
					}
				});
			}
		};
	}
	
]);

app.controller('splashCtrl', ['$rootScope', '$scope', '$location', '$timeout',
	function($rootScope, $scope, $location, $timeout) {
		
		$rootScope.gameStarted = true;
		
		$scope.startGame = function() {
			$rootScope.makeClickSound();
			$rootScope.loaded = 0;
			$location.path("/intro/");
		};
		
	}
]);

app.controller('introCtrl', ['$rootScope', '$scope', '$location', '$storage', '$route',
	function($rootScope, $scope, $location, $storage, $route) { 
		
		var current = 0;
			
		if (!$rootScope.introAssetsLoaded) {
			$location.path('/');
		} else {
			
			$rootScope.ambientSoundName = 'space';
			if($rootScope.aio.sound.music) {
				$rootScope.playAmbientSound(true);
			}
			$scope.background = $rootScope.intro[current].image;
			$scope.message = $rootScope.intro[current].text;
			$scope.label = current == $rootScope.intro.length - 1 ? "Start" : "Next";
			
			$scope.continueGame = function() {
				
				$rootScope.makeClickSound();
				
				if(current < $rootScope.intro.length - 1) {
					++current;
					$scope.background = $rootScope.intro[current].image;
					$scope.message = $rootScope.intro[current].text;
					$rootScope.focusElement("#txtIntro");
				} else {
					$location.path('/space/');
				}
				$scope.label = current == $rootScope.intro.length - 1 ? "Start" : "Next";
			};
			
			$rootScope.focusElement("#txtIntro");
		}	
	}
]);

app.controller('gameInfoCtrl', ['$rootScope', '$scope', '$location', '$storage', '$route',
	function($rootScope, $scope, $location, $storage, $route) { 
		
		var n = $route.current.pathParams.page;
		
		if (!$rootScope.introAssetsLoaded ||
			!isNaN(n) || 
			!n.length) {
			
			$location.path('/');
		} else {
			
			$scope.page = $rootScope.gameinfo[n];
			$rootScope.focusElement('.message');
		}
		
	}
]);

app.controller('settingsCtrl', ['$rootScope', '$scope', '$location', '$timeout',
	function($rootScope, $scope, $location, $timeout) { 
		
		if (!$rootScope.introAssetsLoaded) {
			$location.path('/');
		} else {
			$rootScope.focusElement('#dlgMenu');
		}
	}
]);

app.controller('spaceCtrl', ['$rootScope', '$scope', '$location', '$storage',
	function($rootScope, $scope, $location, $storage) { 
		
		if (!$rootScope.introAssetsLoaded) {
			$location.path('/');
		} else {
			
			$scope.selectPlanet = function(event, index) {
				
				if(event.type === 'click' || event.keyCode === 13 || event.keyCode == 32) {
					$rootScope.makeClickSound();
					if (window.spaceSound) { 
						window.spaceSound.stop();
					}
					$location.path('/planet/info/' + index);
				}
			};
			
			$rootScope.focusElement("#lstPlanets");
		}
	}
]);

app.controller('planetInfoCtrl', ['$rootScope', '$scope', '$location', '$storage', '$route', '$timeout',
	function($rootScope, $scope, $location, $storage, $route, $timeout) { 
		
		var n = parseInt($route.current.pathParams.index),
			snd,
			current = 0,
			hasCharacters;
		
		$scope.planetIndex = n;
		$scope.hasCharacters = !!_.findWhere($rootScope.characters, {planet: n});
		$rootScope.loaded = 0;
		
		if (!$rootScope.introAssetsLoaded ||
			!$rootScope.planets.length ||
			isNaN(n) || 
			n < 0 || 
			n > $rootScope.planets.length) {	
					
			$location.path("/space/");
		} else {
			
			if (!$rootScope.planets[n].assetsLoaded) {
				$rootScope.loadPlanetAssets(n);
			} else {
				$rootScope.loaded = 1;
			}
			
			$scope.completed = $rootScope.aio.planets[n].completed;
			$scope.status = $rootScope.aio.planets[n].status;
			$scope.planet = $rootScope.planets[n];
			$scope.characters = _.where($rootScope.characters, {planet: n});
			$scope.label = $scope.hasCharacters ? ( $rootScope.aio.planets[n].completed ? "Replay": "Continue" ) : "Close";
			
			switch($rootScope.aio.planets[n].status) {
				case 0:
					$scope.planetinfo = $scope.planet.info;
					break;
				case 1:
					$scope.planetinfo = $scope.planet.summary.victory.message;
					break;
				case -1:
					$scope.planetinfo = $scope.planet.summary.defeat.message;
					break;
				
			}
		}
		
		$scope.continueGame = function() {
			
			$rootScope.makeClickSound();
						
			if ($scope.completed) {
				_.extend($rootScope.aio.planets[n], {
					completed: false,
					status: 0,
					allegiance: new Array($scope.characters.length).fill(0),
					currentCharacter: 0,
					toolsUsed: 0
				});
				$rootScope.saveState();
				$location.path('/planet/' + n);
			} else if ($scope.hasCharacters) {
				$rootScope.ambientSoundName = $scope.planet.name;
				if($rootScope.aio.sound.music) {
					$rootScope.playAmbientSound(true);
				}
				$location.path('/planet/' + n);
			} else {
				$location.path('/space/');
			}
			
		};
		
		$scope.exitPlanet = function() {
			$rootScope.makeClickSound();
			$location.path("/space/");
		};
		
		$rootScope.focusElement("#txtPlanetInfo");
	}
]);

app.controller('planetCtrl', ['$rootScope', '$scope', '$location', '$storage', '$route', '$timeout', '$sce',
	function($rootScope, $scope, $location, $storage, $route, $timeout, $sce) { 
		
		var n = parseInt($route.current.pathParams.index), currentToolsUsed = 0,
			timerQuestion, timerTool, snd, allegiance, planetState, lastFocus;
		
		if (!$rootScope.planets.length ||
			isNaN(n) || 
			n < 0 || 
			n > $rootScope.planets.length ||
			!$rootScope.planets[n].assetsLoaded) {
					
			$location.path("/space/");
		} else if ( _.indexOf($rootScope.aio.planets[n].allegiance, 0) < 0 ) {
			$location.path('/planet/' + n + '/boss');
		} else {		
			$scope.planetIndex = n;
			planetState = $rootScope.aio.planets[$scope.planetIndex];
			$scope.planet = $rootScope.planets[$scope.planetIndex];
			$scope.characters = _.where($rootScope.characters, {planet: $scope.planetIndex});
			$scope.characterIndex = planetState.currentCharacter > $scope.characters.length ? 0 : planetState.currentCharacter;
			_.each($scope.characters, function(character, index, list) {
				character.allegiance = planetState.allegiance[index];
			});
			$scope.toolsUsed = planetState.toolsUsed || 0;
			
			setBackground();
			resetToolsAndQuestions();
						
			$scope.currentCharacter = $scope.characters[$scope.characterIndex];
			$scope.currentQuestion = _.findWhere($scope.currentCharacter.questions, {"id": $scope.questionId});
			
			$rootScope.focusElement("#txtSpeechBalloon");
			
			playSpeechSound();
		}
		
		$scope.verifyAnswer = function(index, option) {
			
			var autoMessage = "";
						
			$rootScope.makeClickSound();
			stopSpeechSound();
			
			$scope.questionId = option.next;
			$scope.currentBet = 0;
			currentToolsUsed = 0;
			
			if (option.correct) {
				allegiance = 1;
			} else {
				allegiance = -1;
			}
			//planetState.allegiance[$scope.characterIndex] = allegiance;
			
			$rootScope.saveState();
						
			if (option.feedback.length) {
				
				$rootScope.toggleDialogFocus('#dlgFeedback', true);
				
				if(!$scope.questionId) {
					if (option.correct) {
						autoMessage =  "<h1>You saved " + $scope.currentCharacter.name + " from Captain Plague's Army of the Unearned.</h1>"; 
						if (window.cheerSound) { 
							window.cheerSound.play();
						} else {
							window.cheerSound = createjs.Sound.play('cheer');
						}
					} else {
						autoMessage = "<h1>" + $scope.currentCharacter.name + " has joined Captain Plague's Army of the Unearned.</h1>";
						if (window.booSound) { 
							window.booSound.play();
						} else {
							window.booSound = createjs.Sound.play('boo');
						}
					}
					
				}
				
				$scope.feedback = {
					"message": autoMessage + option.feedback,
					"classname": ""
				};
			} else {
				$scope.goNext();
			}
			$scope.closeHints();
		};
		
		$scope.closeFeedback = function() {
			
			$rootScope.makeClickSound();
			
			$scope.feedback.message = "";
			$scope.currentQuestion = {};
			$rootScope.toggleDialogFocus('#dlgFeedback', false);
			
			if ($scope.completed) { 
				// back to space
				$location.path('/space/');
				$rootScope.ambientSoundName = "space";
				if($rootScope.aio.sound.music) {
					$rootScope.playAmbientSound(true);
				}
			} else {
				timerQuestion = $timeout($scope.goNext, 300);
			}
		};
		
		$scope.goNext = function() {
			
			var summary;
			
			if ($scope.questionId) {
				// show next question
				$scope.currentQuestion = _.findWhere($scope.currentCharacter.questions, {"id": $scope.questionId});
				$scope.tools = _.map($rootScope.tools, _.clone);
				
				$rootScope.focusElement("#txtSpeechBalloon");
				
				playSpeechSound();
				
				setToolTimeout();
				
			} else {
				$scope.characters[$scope.characterIndex].allegiance =  
					planetState.allegiance[$scope.characterIndex] = allegiance;
								
				if (_.where($scope.characters, {allegiance: -1}).length == $scope.characters.length) {
					// you lost
					summary = $scope.planet.summary;
					
					$rootScope.toggleDialogFocus('#dlgFeedback', true);
					
					$scope.feedback = {
						"image": summary.defeat.image,
						"message": summary.defeat.message,
						"classname": "defeat"
					};
					$rootScope.aio.planets[n].currentCharacter = 0;
					$rootScope.aio.planets[n].completed = true;
					$rootScope.aio.planets[n].status = -1;
					$rootScope.saveState();
					
					if (window.laughterSound) { 
						window.laughterSound.play();
					} else {
						window.laughterSound = createjs.Sound.play('laughter');
					}

					planetState.score = $scope.goodarmy.length + ':' + $scope.evilarmy.length;
					$scope.completed = true;
					
				} else if (_.where($scope.characters, {allegiance: 1}).length == $scope.characters.length) {
					// you won
					summary = $scope.planet.summary;
					
					$rootScope.toggleDialogFocus('#dlgFeedback', true);
					
					$scope.feedback = {
						"image": summary.victory.image,
						"message": summary.victory.message,
						"classname": "victory"
					};
					$rootScope.aio.planets[n].currentCharacter = 0;
					$rootScope.aio.planets[n].completed = true;
					$rootScope.aio.planets[n].status = 1;
					$rootScope.saveState();
					
					if (window.victorySound) { 
						window.victorySound.play();
					} else {
						window.victorySound = createjs.Sound.play('victory');
					}
					
					planetState.score = $scope.goodarmy.length + ':' + $scope.evilarmy.length;
					$scope.completed = true;
					
				} else if ($scope.characterIndex >= $scope.characters.length - 1) {
					// if last character
					stopSpeechSound();
					$location.path('/planet/' + $scope.planetIndex + '/boss');
				} else {
					// move to next character
					setBackground();
					$('.used').removeClass('used');
					$scope.currentBet = 0;
					$scope.tools = _.map($rootScope.tools, _.clone);
					$scope.characterIndex = $scope.characterIndex > $scope.characters.length - 2 ? $scope.characterIndex : ++$scope.characterIndex;
					$scope.currentCharacter = $scope.characters[$scope.characterIndex];
					
					$rootScope.aio.planets[$scope.planetIndex].currentCharacter = $scope.characterIndex;
					
					$scope.questionId = 1;
					$scope.currentQuestion = _.findWhere($scope.currentCharacter.questions, {"id": $scope.questionId});
					
					$rootScope.focusElement("#txtSpeechBalloon");
					
					playSpeechSound();
				}
			}
			$rootScope.saveState();
			$timeout.cancel(timerQuestion);
			
			
		};
		
		$scope.tools = _.map($rootScope.tools, _.clone); 
		
		$scope.getHint = function(e, n) {
			var tool = $scope.tools[n];
			
			$rootScope.makeClickSound();
			
			lastFocus = e.currentTarget;
			
			$rootScope.toggleDialogFocus('#dlgHint', true);
			
			$scope.hintId = n;
			$scope.currentHint = {
				"tool": tool,
				"message": $scope.currentQuestion.hints[n],
				"value": tool.value
			}
			
			$scope.currentBet += tool.value;
			if (tool.value) {
				$scope.toolsUsed++;
				currentToolsUsed++;
			}
			tool.value = 0;
			
			$timeout.cancel($scope.toolTimer);
			$('.blinking').removeClass('blinking');
			
			$rootScope.saveState();
		}
		
		$scope.closeHints = function() {
			
			$rootScope.makeClickSound();
			
			$rootScope.toggleDialogFocus('#dlgHint', false);
			
			if(lastFocus) {
				$rootScope.focusElement( $(lastFocus) );
				lastFocus = null;
			}
			
			$scope.hintId = null;
			$scope.currentHint = {
				"tool": {},
				"message": "",
				"value": 0
			};
		}
		
		function setToolTimeout() {
			$scope.toolTimer = $timeout(function () {
				$('.tool').find('.icon').addClass('blinking');
			}, 5000);
		}
		
		setToolTimeout();
		
		$scope.exitPlanet = function() {
			$rootScope.makeClickSound();
			stopSpeechSound();
			$location.path("/space/");
			$rootScope.ambientSoundName = "space";
			if($rootScope.aio.sound.music) {
				$rootScope.playAmbientSound(true);
			}
		};
		
		function setBackground() {
			$scope.bgImage = _.random(0, 1) ? $scope.planet.background.night : $scope.planet.background.day
			$scope.bgPosition = _.random(-300, 0);
		}
		function resetToolsAndQuestions() {
			$scope.failed = false;
			$scope.currentBet = 0;
			$scope.points = 0;
			$scope.questionId = 1;
			$scope.feedback = {
				"message": "",
				"classname": ""
			};
			$scope.hintId = null;
			$scope.currentHint = {
				"tool": {},
				"message": "",
				"value": 0
			};
		}
		function playSpeechSound() {
			if($rootScope.aio.sound.speech) {
				window.voiceSound = createjs.Sound.play($scope.currentCharacter.name + "_" + $scope.currentCharacter.planet + "_" + $scope.currentQuestion.id, { volume: 1 });
			}
			
		}
		function stopSpeechSound() {
			if (window.voiceSound) { 
				window.voiceSound.stop();
			}
			
		}
		
	}
]);

app.controller('bossCtrl', ['$rootScope', '$scope', '$location', '$storage', '$route', '$timeout',
	function($rootScope, $scope, $location, $storage, $route, $timeout) { 
		
		var n, planetState, snd, currentIndex, timerQuestion, questions;
		
		$rootScope.stopAmbientSound();
		
		n = parseInt($route.current.pathParams.index);
		
		if (!$rootScope.introAssetsLoaded ||
			!$rootScope.planets.length || 
			isNaN(n) || 
			n < 0 || 
			n > $rootScope.planets.length) {
					
			$location.path("/space/");
		} else {
			planetState = $rootScope.aio.planets[n];
			$scope.planet = $rootScope.planets[n];
			$scope.planetIndex = n;
			$scope.questions = _.where($rootScope.bossQuestions, {"planet": n});
			$scope.characters = _.where($rootScope.characters, {planet: n});
			_.each($scope.characters, function(character, index, list) {
				character.allegiance = planetState.allegiance[index];
			});
			$scope.attempts = questions = Math.floor($scope.characters.length / 2);
		}
		
		currentIndex = 0;
		//$scope.currentQuestion = $scope.questions[currentIndex];
		$scope.completed = false;
		$scope.feedback = {
			"image": "",
			"message": "<h2>You've lost some students to Captain Plague's League of the Unearned. Fear not, you can still save them and the planet by battling Captain Plague directly!</h2>",
			"classname": "bossintro"
		};
		$rootScope.toggleDialogFocus('#dlgFeedback', true);
		if (window.angerSound) { 
			window.angerSound.play();
		} else {
			window.angerSound = createjs.Sound.play('anger');
		}
		
		$scope.verifyAnswer = function(index, option) {
			
			var ind;
			
			$rootScope.makeClickSound();
			
			if (option.correct) {
				
				ind = _.findIndex($scope.characters, {allegiance: -1});
				$scope.characters[ind].allegiance = 1;
				
				if (window.angerSound) {
					window.angerSound.play();
				} else {
					window.angerSound = createjs.Sound.play('anger');
				}
			} else {
				
				ind = _.findLastIndex($scope.characters, {allegiance: 1})
				$scope.characters[ind].allegiance = -1;
				
				if (window.laughterSound) {
					window.laughterSound.play();
				} else {
					window.laughterSound = createjs.Sound.play('laughter');
				}
			}
			
			planetState.allegiance = _.map($scope.characters, function(item) { return item.allegiance });
			planetState.score = $scope.goodarmy.length + ':' + $scope.evilarmy.length;
			$rootScope.saveState();
			
			$scope.attempts--;
			currentIndex++;
			
			if (option.feedback.length) {
				// show feedback
				$rootScope.toggleDialogFocus('#dlgFeedback', true);
				
				$scope.feedback = {
					"image": "",
					"message": option.feedback,
					"classname": ""
				};
			} else {
				$scope.currentQuestion = {};
				timerQuestion = $timeout($scope.goNext, 500);
			}
		};
		
		$scope.goNext = function() {
			var character;
			
			if (!$scope.attempts || _.indexOf(planetState.allegiance, 1) < 0 || _.indexOf(planetState.allegiance, -1) < 0) {
				showSummary();
			} else {
				//$scope.currentQuestion = $scope.questions[currentIndex];
				ind = _.findIndex($scope.characters, {allegiance: -1});
				character = $scope.characters[ind];
				$scope.currentQuestion = character.boss[0];
				$rootScope.focusElement(".question");
			}
		}
		
		$scope.closeFeedback = function() {
			$rootScope.makeClickSound();
			
			$rootScope.toggleDialogFocus('#dlgFeedback', false);
			
			$scope.feedback = {
				"image": "",
				"message": "",
				"classname": ""
			};
			if ($scope.completed) {
				$rootScope.ambientSoundName = 'space';
				$location.path("/space/");
			} else {
				$scope.currentQuestion = {};
				timerQuestion = $timeout($scope.goNext, 500);
			}
		};
		
		function showSummary () {
			var summary, all, you;
			
			$rootScope.toggleDialogFocus('#dlgFeedback', true);
			
			summary = $scope.planet.summary;
			all = $scope.characters.length;
			you = _.where($scope.characters, {allegiance: 1}).length;
			
			if (you < all / 2) {
				
				$scope.feedback = {
					"image": summary.defeat.image,
					"message": summary.defeat.message,
					"classname": "defeat"
				};
				planetState.completed = true;
				planetState.status = -1;
				$rootScope.saveState();
				
			} else {
				
				$scope.victory = true;
				$scope.feedback = {
					"image": summary.victory.image,
					"message": summary.victory.message,
					"classname": "victory"
				};
				planetState.completed = true;
				planetState.status = 1;
				$rootScope.saveState();
				
				if (window.victorySound) {
					window.victorySound.play();
				} else {
					window.victorySound = createjs.Sound.play('victory');
				}
				planetState.currentCharacter = 0;
			}
			$scope.completed = true;
			$rootScope.saveState();
		};
		
	}
]);

app.controller('multitextCtrl', ['$rootScope', '$scope', '$location', 
	function($rootScope, $scope, $location) {
		
	}
]).directive('multitext', [ 
	function () {
		return {
			restrict: 'AE',
			replace: true,
			scope: {
		    	textArray: '=textarray'
		    },
			link: function ($scope, $elem, attrs) {
			
				$scope.first = 0;
				$scope.last = $scope.textArray.length - 1;
				$scope.current = 0;
				
				$scope.next = function () {
					
					$scope.$root.makeClickSound();
					
					$scope.ngModel = $scope.ngModel ? $scope.ngModel : ($scope.current == $scope.last - 1);
					if ($scope.current < $scope.last) {
						$scope.current++;
					}
				}
				$scope.prev = function () {
					
					$scope.$root.makeClickSound();
					
					if ($scope.current > $scope.first) {
						$scope.current--;
					}
				}	
			},
			templateUrl:'_/tpl/multitext.tpl.html'
		}
	}
]);

///*  UTILS  *///


utils.factory('$storage', ['$window', '$cookies', function($window, $cookies) {
	var expiry_days = 10;
	
	function isLocalStorageAvailable() {
		var str = 'test';
		try {
			localStorage.setItem(str, str);
			localStorage.removeItem(str);
			return true;
		} catch(e) {
			return false;
		}
	}

	return {
		set: function(key, value) {
			var d = new Date();
			if (isLocalStorageAvailable()) {
				$window.localStorage[key] = value;
			} else {
				$cookies(key, value, {expires: d.setDate(expiry_days)});
			}
		},
		get: function(key) {
			var r = (isLocalStorageAvailable()) ? $window.localStorage[key] : $cookies.get(key);
			return r;
		},
		setObject: function(key, value) {
			var d = new Date(),
				o = JSON.stringify(value);
			if (isLocalStorageAvailable()) {
				$window.localStorage[key] = o;
			} else {
				$cookies.putObject(key, o, {expires: d.setDate(expiry_days)});
			}
		},
		getObject: function(key) {
			var r = (isLocalStorageAvailable()) ? $window.localStorage[key] : $cookies.getObject(key);
			return r ? JSON.parse(r) : false;
		},
		remove: function(key) {
			if (isLocalStorageAvailable()) {
				$window.localStorage.removeItem(key);
			} else {
				$cookies.remove[key];
			}
		}
	}
}]);

function arrayFillMethod() {
	if (!Array.prototype.fill) {
		Object.defineProperty(Array.prototype, 'fill', {
			value: function(value) {
				
				// Steps 1-2.
				if (this == null) {
					throw new TypeError('this is null or not defined');
				}
				
				var O = Object(this);
				
				// Steps 3-5.
				var len = O.length >>> 0;
				
				// Steps 6-7.
				var start = arguments[1];
				var relativeStart = start >> 0;
				
				// Step 8.
				var k = relativeStart < 0 ?
					Math.max(len + relativeStart, 0) :
					Math.min(relativeStart, len);
				
				// Steps 9-10.
				var end = arguments[2];
				var relativeEnd = end === undefined ?
					len : 
					end >> 0;
				
				// Step 11.
				var final = relativeEnd < 0 ?
					Math.max(len + relativeEnd, 0) :
					Math.min(relativeEnd, len);
				
				// Step 12.
				while (k < final) {
					O[k] = value;
					k++;
				}
				
			// Step 13.
			return O;
			}
		});
	}
}

function generateUUID() {
	var r, d;
	d = new Date().getTime();
	if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
		d += performance.now(); //use high-precision timer if available
	}
	r = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
	return r;
};

function getBrowser() {
	var browser, isIE;
	
	isIE = /*@cc_on!@*/false || !!document.documentMode;
	
	if ( (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0 ) {
		browser = "Opera";
	} else if ( typeof InstallTrigger !== 'undefined' ) {
		browser = "Firefox";
	} else if ( /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification)) ) {
		browser = "Safari";
	} else if ( isIE ) {
		browser = "Internet Explorer";
	} else if ( !isIE && !!window.StyleMedia ) {
		browser = "Edge";
	} else if ( !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime) ) {
		browser = "Chrome";
	} else {
		browser = "Unknown browser";
	}
	return browser;
}

